/* TUGAS 2 CONDITIONAL SOAL A */
console.log("======SOAL A========");
var nama = 'Jane';
var peran = 'Penyihir';

if(nama =='')
{
    console.log("Nama harus diisi !");
}
else {
    if(peran=='')
    {
        console.log("Halo " + nama +", Pilih peranmu untuk memulai game!");
    }
    else if(peran == 'Penyihir')
    {
        console.log("Selamat Datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama +", kamu dapat melihat siapa yang menjadi werewolf!");
    }
    else if(peran == 'Guard')
    {
        console.log("Selamat Datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran == 'Werewolf')
    {
        console.log("Selamat Datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama +", Kamu akan memakan mangsa setiap malam!");
    }
}


/* TUGAS 2 CONDITIONAL SOAL B */
console.log("=========SOAL B===========");

var tanggal = 65;
var bulan = 5;
var tahun = 170;

if(tanggal >=1 && tanggal <=31){
    if(tahun>=1900 && tahun <= 2200) {
        switch(bulan) {
            case 1 :  { bulan = "Januari";break;}
            case 2 :  { bulan = "Februari";break;}
            case 3 :  { bulan = "Maret";break;}
            case 4 :  { bulan = "April";break;}
            case 5 :  { bulan = "Mei";break;}
            case 6 :  { bulan = "Juni";break;}
            case 7 :  { bulan = "Juli";break;}
            case 8 :  { bulan = "Agustus";break;}
            case 9 :  { bulan = "September";break;}
            case 10 :  { bulan = "Oktober";break;}
            case 11 :  { bulan = "November";break;}
            case 12 :  { bulan = "Desember";break;}
            default : { console.log("Bulan tidak valid");}
        }        
    }
    else {
        console.log("Tahun tidak valid");
    }
}
else {
    console.log("Tanggal tidak valid");
}

    console.log(tanggal+" "+bulan+" "+tahun);

