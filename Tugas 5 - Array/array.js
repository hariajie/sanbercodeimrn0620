console.log("----SOAL NO. 1----")

function range(startNum='', finishNum='') {
    var number = [];
    if(startNum=='' || finishNum=='') {
        return (-1);
    }
    if(startNum < finishNum) {
        for(startNum;startNum<=finishNum;startNum++) {
            number.push(startNum);
        }
        return number;
    }
    else {
        for(startNum;startNum>=finishNum;startNum--) {
            number.push(startNum);
        }
        return number;
    }
}

console.log(range(1,10));
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

console.log("");
console.log("----SOAL NO. 2----")

function rangeWithStep(startNum='', finishNum='', step=1) {
    var number = [];
    if(startNum=='' || finishNum=='') {
        return (-1);
    }
    if(startNum < finishNum) {
        while(startNum<=finishNum) {
            number.push(startNum);
            startNum+=step;
        }
        return number;
    }
    else {
        while(startNum>=finishNum) {
            number.push(startNum);
            startNum-=step;
        }
        return number;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("");
console.log("----SOAL NO. 3----")

function sum(startNum='', finishNum='', step=1) {
    var number=[];
    var total=0;

    if(startNum=='') {
        return 0;
    }
    else if (finishNum=='') {
        return startNum;
    }
    else {
        number = rangeWithStep(startNum, finishNum, step);
        for(let i =0;i<number.length;i++) {
            total += number[i];
        }

        return total;
    }
       
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("");
console.log("----SOAL NO. 4----")

function dataHandling(param) {
    var cetak='';
    for(let i=0;i<param.length;i++) {
        cetak = cetak + 
                "Nomor ID: "+ param[i][0]+ "\n" +
                "Nama Lengkap: "+ param[i][1]+"\n" +
                "TTL: " + param[i][2]+ " "+param[i][3] + "\n" +
                "Hobi: "+ param[i][4] + "\n\n";
    }
    return cetak;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input));

console.log("----SOAL NO. 5----")

function balikKata(param) {
    var balik="";
    for(let i=param.length-1;i>=0;i--) {
        balik = balik+param.charAt(i);
    }
    return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("");
console.log("----SOAL NO. 6----");

function ambil_bulan(month) {
   
    var bulan = '';
    switch(month) {
        case '01' : {bulan = "Januari"; break;}
        case '02' :  { bulan = "Februari";break;}
        case '03' :  { bulan = "Maret";break;}
        case '04' :  { bulan = "April";break;}
        case '05' :  { bulan = "Mei";break;}
        case '06' :  { bulan = "Juni";break;}
        case '07' :  { bulan = "Juli";break;}
        case '08' :  { bulan = "Agustus";break;}
        case '09' :  { bulan = "September";break;}
        case '10' :  { bulan = "Oktober";break;}
        case '11' :  { bulan = "November";break;}
        case '12' :  { bulan = "Desember";break;}
        default : { bulan='';break;}
    }
    return bulan;
}
function dataHandling2(param){
    var output='';
    
    //row 1
    param.splice(1, 1, "Roman Alamsyah Elsharawy");
    param.splice(2, 1, "Provinsi Bandar Lampung");
    param.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(param);
    //output = output + param + "\n";

    //row2
    var tgl = param[3].split("/");
    var bln = ambil_bulan(tgl[1]);
    //output = output + bln  +"\n";
    console.log(bln);

    //row3
    tgl_sort = tgl.sort(function(value1, value2){return value2 - value1});
    //output = output + tgl + "\n";
    console.log(tgl_sort);

    //row4
    tgl_join = param[3].split("/").join("-");
    console.log(tgl_join);

    //row5
    var nama = param[1];
    nama = nama.slice(0,15);
    console.log(nama);
    
    //return output;



    
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 