import React, {Component} from 'react';
import { StatusBar } from 'expo-status-bar';
import {View, Text, StyleSheet, Image, TouchableOpacity, TouchableNativeFeedback, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/VideoItem';
import data from './data.json';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require("./images/logo.png")} style={{height: 22, width: 100}}></Image>
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name="search" size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name="account-circle" size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.body}>
                
                        <FlatList 
                        data={data.items}
                        renderItem={(video)=><VideoItem video={video.item}/>}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor: "#E5E5E5"}} />}
                        />
                    </View>
                    <View style={styles.tabBar}>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="home" size={25} style={{color: 'red'}}/>
                            <Text style={styles.homeTitle}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="whatshot" size={25}/>
                            <Text style={styles.tabTitle}>Trending</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="subscriptions" size={25}/>
                            <Text style={styles.tabTitle}>Subscriptions</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="folder" size={25}/>
                            <Text style={styles.tabTitle}>Library</Text>
                        </TouchableOpacity>
                    </View>
                   
            </View>       
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    navBar : {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop:15
    },
    rightNav : {
        flexDirection: 'row',
    },
    navItem : {
        marginLeft: 25
    },
    body: {
        flex: 1
    },
    tabBar : {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderTopColor: '#E5E5E5',
        flexDirection:'row',
        justifyContent:'space-around'
    },
    tabItem : {
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabTitle: {
        fontSize: 11,
        color: '#3c3c3c'
    },
    homeTitle : {
        fontSize: 11,
        color: '#ff0000'
    }

});