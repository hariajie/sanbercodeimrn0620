import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from './components/skillItems';
import data from './skillData.json';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{ width: 110, height: 40 }} />
        </View>
        <View style={styles.header}>
            <Icon style={styles.people} name="account-circle" size={40} />
            <View style={styles.cheader}>
                <Text> Hai,</Text>
                <Text style={{color: '#003366', fontSize:16,}}> Mukhlis Hanafi</Text>
            </View>
        </View>
        <View style={styles.body}>
            <View>
                <Text style={{color: '#003366', fontSize:30,}}> SKILL</Text>
                <View style={styles.delimiter}></View>
            </View>
            <View style={styles.bodyNav}>
                <View style={styles.bodyNavTitle}>
                    <Text style={styles.bodyTitle}> Library/Framework</Text>
                    <Text style={styles.bodyTitle}> Bahasa Pemrograman</Text>
                    <Text style={styles.bodyTitle}> Teknologi</Text>
                </View>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id.toString()}
                    ItemSeparatorComponent={()=><View style={{height:0.3,backgroundColor:'#E5E5E5'}}/>}
                    />
                   
            </View>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginTop: 30
  },
  header:{
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  cheader:{
    flexDirection: 'column',
  },
  cheaderText:{
    textAlign:'left',
  },
  delimiter:{
    marginHorizontal:0,
    height:2,
    backgroundColor:'#3EC6FF'
  },

  people:{
    textAlign:'left',
    marginLeft: 16,
    color : '#3EC6FF'
  },
  body: {
    flex: 1,
    marginLeft: 10,
    marginRight:10,
  },
  bodyNavTitle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding : 5,
    justifyContent: 'space-between'
  },
  bodyTitle: {
    color: '#003366', 
    fontSize:12, 
    backgroundColor: '#B4E9FF', 
    padding:5, 
    margin : 3,
    borderRadius:8,
  },

  
});
