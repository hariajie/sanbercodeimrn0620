import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import Login from './LoginScreen'
import SkillScreen from './SkillScreen'
import Project from './ProjectScreen'
import Add from './AddScreen'
import About from './AboutScreen'
import { Root } from './RootScreen'

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const RootStack = createStackNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();

const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={SkillScreen} />
    </SkillStack.Navigator>
)

const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name="Project" component={Project} />
    </ProjectStack.Navigator>
)

const AddStackScreen = () => (
    <AddStack.Navigator>
        <AddStack.Screen name="Add" component={Add} />
    </AddStack.Navigator>
)

const AboutScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={About} />
    </AboutStack.Navigator>
)

const TabsScreen = () => (
    <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={SkillStackScreen} />
      <Tabs.Screen name="Project" component={ProjectStackScreen} />
      <Tabs.Screen name="Add" component={AddStackScreen} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen} />
        <Drawer.Screen name="About" component={AboutScreen} />
    </Drawer.Navigator>
)

const RootStackScreen = () => {
    return (
      <RootStack.Navigator>
        <RootStack.Screen name="Root" component={Root} />
        <RootStack.Screen 
          name="Login" 
          component={Login} />
        <RootStack.Screen 
          name="Drawer" 
          component={DrawerScreen}  
        />
      </RootStack.Navigator>
    )
}

export default function App15() {
    return (
      <NavigationContainer>
          <RootStackScreen />
      </NavigationContainer>
    );
}