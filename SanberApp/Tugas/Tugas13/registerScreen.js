import React from 'react';
import {
  StyleSheet,
  View, 
  Image,
  Text,
  TextInput,
  TouchableOpacity
}from 'react-native';


export default class App extends React.Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require('./images/logo.png')}/>
        </View>
        <View style={styles.regisContainer}>
          <Text style={styles.regisText}>Register</Text>
        </View>
        <View style={styles.usernameContainer}>
          <Text style={styles.miniText}>Username</Text>
          <TextInput style={styles.inputbox} />
        </View>
        <View style={styles.usernameContainer}>
          <Text style={styles.miniText}>Email</Text>
          <TextInput style={styles.inputbox} />
        </View>
        <View style={styles.passwordContainer}>
          <Text style={styles.miniText}>Password</Text>
          <TextInput secureTextEntry={true} style={styles.inputbox} />
        </View>
        <View style={styles.passwordContainer}>
          <Text style={styles.miniText}>Ulangi Password</Text>
          <TextInput secureTextEntry={true} style={styles.inputbox} />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonDaftar}>
            <Text style={styles.buttonText}>Daftar</Text>
          </TouchableOpacity>
          <Text style={{fontSize: 20, color:'#3EC6FF', marginVertical:8}}> atau </Text>
          <TouchableOpacity style={styles.buttonMasuk}>
            <Text style={styles.buttonText}>Masuk?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  logoContainer:{
    paddingTop: 45
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 16    
  },
  regisContainer:{
    alignItems: 'center',
    marginVertical: 28
  },
  buttonMasuk:{
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: '#003366',
    width: 120,
    height: 35
  },
  buttonDaftar:{
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: '#3EC6FF',
    width: 120,
    height: 35
  },
  buttonText : {
    fontSize: 20,
    color: 'white',
  },
  regisText:{
    fontSize: 24,
    color: '#003366'
  },
  inputbox : { 
    height: 40, 
    borderColor: '#003366', 
    borderWidth: 1,
    paddingLeft: 10
  },
  miniText: {
    fontSize: 14
  }, 
  usernameContainer:{
    marginBottom: 16,
    paddingHorizontal: 40
  },
  passwordContainer:{
    marginBottom: 16,
    paddingHorizontal: 40
  }
});