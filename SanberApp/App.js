import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';

import { StyleSheet, Text, View } from 'react-native';

// import Login from './Tugas/Tugas13/loginScreen';
// import About from './Tugas/Tugas13/aboutScreen';
// import Register from './Tugas/Tugas13/registerScreen';
// import ToDoApp from './Tugas/Tugas14/ToDoApp';
// import SkillScreen from './Tugas/Tugas14/skillScreen';
// import Tugas15_1 from './Tugas/Tugas15/index';
// import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';
import LatihanAPI from './Latihan/api/latihanAPI';
import RESTApi from './Tugas/RESTful/index';

export default function App() {
  return (

    <View style={styles.container}>
      <StatusBar />

      {/* <Login/> */}
      {/* <About/> */}
      {/* <ToDoApp /> */}
      {/* <Register/> */}
      {/* {<SkillScreen />} */}

      {/* <Tugas15_1 /> */}
      {/* <TugasNavigation /> */}
      {/* <Quiz3 /> */}
      {/* <LatihanAPI /> */}
      <RESTApi />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
