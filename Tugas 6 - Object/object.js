function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
    var obj = {};
    // Code di sini 
    for(let i=0; i< arr.length;i++) {
        obj.firstName = arr[i][0];
        obj.lastName = arr[i][1];
        obj.gender = arr[i][2];

        if(arr[i][3] > thisYear || arr[i][3] == null) {
          
            obj.age = "Invalid Birth Year";
        }
        else {
            obj.age = thisYear - arr[i][3];
        }
        console.log((i+1) +". "+ obj.firstName+" "+obj.lastName+": ", obj);
    }
   
}
 
// Driver Code
console.log("---SOAL NO. 1----")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 


console.log("---SOAL NO. 2----")
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var shopObj={};
    var list = [];
    var uangAwal = money;
    if(!memberId || !money) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }else {
        while(money>=50000) {
            if(money>=1500000) {
                    list.push("Sepatu Stacattu")
                    money -= 1500000
            }
            else if(money >= 500000) {
                list.push("Baju Zoro")
                money -= 500000
            }
            else if(money >= 250000) {
                list.push("Baju H&N")
                money -= 250000
            }
            else if(money >= 50000) {
                list.push("Casing HP")
                money -= 50000; break;
            }
        }

        shopObj = {
            memberId : memberId,
            money : uangAwal,
            listPurchased : list,
            changeMoney : money
        };

        return shopObj;
    }

 }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("----SOAL NO. 3----");
function naikAngkot(arrPenumpang) {
    //your code here
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var bayar = 0;
    var angkotObj = {};
    var i=0;
    var showArr = [];

    if(arrPenumpang.length == 0 ) {
        return "[]";
    }
    else {
        while(i < arrPenumpang.length) {
            for (let x = rute.indexOf(arrPenumpang[i][1]); x < rute.indexOf(arrPenumpang[i][2]); x++ ) {
                bayar += 2000;
            }
            angkotObj.penumpang = arrPenumpang[i][0];
            angkotObj.naikDari = arrPenumpang[i][1];
            angkotObj.tujuan = arrPenumpang[i][2];
            angkotObj.biaya = bayar;
            console.log(angkotObj);
            angkotObj ={};
            bayar=0;
            i++;
        }    
    } 
}
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  console.log(naikAngkot([])); //[]